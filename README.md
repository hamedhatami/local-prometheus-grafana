# local-prometheus-grafana

This docker-compose is prepared for testing the observability locally utilizing Grafana + Prometheus + OpenTelemrtry Collectors

## Prerequisite

Docker Compose is required for you to be able to run the command

## Getting started

In order to get the monitoring system up and running locally just run following command

```
cd local-prometheus-grafana
docker-compose -f prometheus-grafana.yaml up -d
```

## Getting down

```
cd local-prometheus-grafana
docker-compose -f prometheus-grafana.yaml down -v
```

## Usage

In order for you to build a dashboard in the Grafana GUI just open a browser and go to http://localhost:3000 so that in the login page give admin (username) and grafana (password) and start off by building your dashbaord.
